![Build Status](https://gitlab.com/pages/hakyll/badges/master/build.svg)

---

This is my an GitLab page created using [Hakyll](https://jaspervdj.be/hakyll/) as a template.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---
## Build Locally
To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download the project from [GitLab](https://gitlab.com/pages/hakyll) if you plan to build a [GitLab Page](https://docs.gitlab.com/ee/user/project/pages/) or follow the [tutorial](https://jaspervdj.be/hakyll/tutorials/github-pages-tutorial.html) from the offical website for GitHub Page.

2. Install Hakyll.

3. Generate the website: `stack exec site build`

4. Preview your project: `stack exec site watch`

5. Add content