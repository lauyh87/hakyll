---
title: About
---
 I am Richard, currently working as a Java Developer in a F&B Solution company.
 My primary job is doing bug fixing for the backend system written in Java Spring Boot,
 and involved in system integration with Android based point of sales system.

 I am passionate to explore new technologies and get the hand dirty on trying the new stuff.
 This is my first gitlab page generate using hakyll, an static site generator written in Haskell.
